package gitlab;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test(groups = {"testGroup1"})
public class ExampleTest 
{

    @Test
    public void assertTrue()
    {
        Assert.assertTrue(true);
    }

    @Test
    public void assertEquals() {
      int a=3;
      int b=3;
      Assert.assertEquals(a+b, 6 , "MY ERROR: not Equals");
    }

    @Test
    public void assertFalse()
    {
        Assert.assertTrue(2+2==5);
    }
}
